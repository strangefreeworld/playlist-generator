import argparse
import random
import sqlite3
import collections
from dataclasses import dataclass

@dataclass
class track:
    path: str
    albumname: str
    artist: str
    length: int
    title: str

#TrackInfo = collections.namedtuple("TrackInfo", "path albumname artist length title")


# simple SQLite context manager from https://gist.github.com/miku/6522074
class sqlopen:
    """
    Simple CM for sqlite3 databases. Commits everything at exit.
    """

    def __init__(self, path):
        self.path = path
        self.conn = None
        self.cursor = None

    def __enter__(self):
        self.conn = sqlite3.connect(self.path)
        self.cursor = self.conn.cursor()
        return self.cursor

    def __exit__(self, exc_class, exc, traceback):
        self.conn.commit()
        self.conn.close()


def album_name(album_id, album_data):
    return album_data[album_id] if album_id in album_data else None


class TrackInfoGenerator:
    def __init__(self, albums, artist):
        self.albums = albums
        self.artist = artist

    def __call__(self, trackdata):
        alname = album_name(trackdata[1], self.albums)
        return track(trackdata[0],
                     alname,
                     self.artist,
                     int(trackdata[3] / 1000),
                     trackdata[4])


def generate_playlist(args):
    trackdata = []
    albums = []
    with sqlopen(args.db) as dbcursor:
        trackquery = dbcursor.execute(
            f"SELECT filename, albumID, artistID, length, title from Tracks where artistID in (SELECT artistID from artists where cissearch='{args.band.lower()}')"
        )
        tracks = trackquery.fetchall()
        album_set = {item[1] for item in tracks}
        album_list = ", ".join([str(i) for i in album_set])
        album_data = dbcursor.execute(
            f"SELECT albumID, name from albums where albumID in ({album_list});"
        )
        albums = {item[0]: item[1] for item in album_data.fetchall()}
        ti_generator = TrackInfoGenerator(albums, args.band.title())
        trackdata = list(map(ti_generator, tracks))
    playlist = random.sample(trackdata, args.length)
    with open(args.output, "w") as plfile:
        plfile.write("#EXTM3U\n")
        for track in playlist:
            plfile.write(f"#EXTINF:{track.length},{track.artist} - {track.title}\n")
            plfile.write(f"{track.path}\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="plg", description=("Generate a random playlist for an artist")
    )
    parser.add_argument("db", help="Database to use")
    parser.add_argument("band", help="Band to use for playlist generation")
    parser.add_argument(
        "--length",
        "-n",
        help="Length of playlist",
        type=int,
        required=False,
        default=10,
    )
    parser.add_argument(
        "--output", "-o", help="Playlist output", required=False, default="playlist.m3u"
    )
    args = parser.parse_args()
    generate_playlist(args)
